### [No instance with class methods](https://github.com/linhchauatl/ruby_threads/blob/master/class_methods.rb) vs. [Singleton design pattern](https://github.com/linhchauatl/ruby_threads/blob/master/singleton_instance.rb)

Recently, there are a lot of debates over the Internet and within professional groups about what is the better way to implement a Service in Object Oriented Programming: [No instance with class methods](https://github.com/linhchauatl/ruby_threads/blob/master/class_methods.rb) or [Singleton design pattern](https://github.com/linhchauatl/ruby_threads/blob/master/singleton_instance.rb)?

At the first glance, an implementation of [No instance with class methods](https://github.com/linhchauatl/ruby_threads/blob/master/class_methods.rb) will be used like this:
```ruby
RunProcess.execute('Some parameters')
```

An implementation of [Singleton design pattern](https://github.com/linhchauatl/ruby_threads/blob/master/singleton_instance.rb) will be used like this:
```ruby
instance = RunProcessSingleton.instance
instance.execute('Some parameters')
```
or
```ruby
RunProcessSingleton.instance.execute('Some parameters')
```

No big deal, you might think. But behind the scene, it is much more complicated.

Let run the program [test_threads.rb](https://github.com/linhchauatl/ruby_threads/blob/master/test_threads.rb) in [this repo](https://github.com/linhchauatl/ruby_threads.git) with this command: `ruby test_threads.rb`

You will see output similar to this:
```
$ruby test_threads.rb

1) Test Class methods ...
Started at 2018-01-13 21:09:56 -0600
Start RunProcess.execute Thread 1: 2018-01-13 21:09:56 -0600
Start RunProcess.execute Thread 0: 2018-01-13 21:09:56 -0600
End RunProcess.execute Thread 1: 2018-01-13 21:10:01 -0600
***** client in thread 'Thread 1' value = <Client for Thread 1>
End RunProcess.execute Thread 0: 2018-01-13 21:10:01 -0600
***** client in thread 'Thread 0' value = <Client for Thread 0>
End at 2018-01-13 21:10:01 -0600
Duration: 5

2) Test Singleton instance ...
Started at 2018-01-13 21:10:01 -0600
Start instance.execute Thread 0: 2018-01-13 21:10:01 -0600
Start instance.execute Thread 1: 2018-01-13 21:10:01 -0600
End instance.execute Thread 0: 2018-01-13 21:10:06 -0600
***** @client in thread 'Thread 0' value = <Client for Thread 1>
End instance.execute Thread 1: 2018-01-13 21:10:06 -0600
***** @client in thread 'Thread 1' value = <Client for Thread 1>
End at 2018-01-13 21:10:06 -0600
Duration: 5

```

In both methods `execute` of the 2 implementations, there is a delay of 5 seconds between start and end, to illustrate long execution time of a service.<br/>
In the program [test_threads.rb](https://github.com/linhchauatl/ruby_threads/blob/master/test_threads.rb), to test each implementation, it starts 2 threads concurrently.

According to 2 output `Duration: 5`, we can see that in both cases, the 2 concurrent test threads are executed in parallel at the same time, so they end after 5 seconds for both implementations.

However, here is the real trouble in the case of using [Singleton instance](https://github.com/linhchauatl/ruby_threads/blob/master/singleton_instance.rb): Watch those two lines with `*****` in two cases:
```
1) Test Class methods ...
...
***** client in thread 'Thread 1' value = <Client for Thread 1>
...
***** client in thread 'Thread 0' value = <Client for Thread 0>
...

2) Test Singleton instance ...
...
***** @client in thread 'Thread 0' value = <Client for Thread 1>
...
***** @client in thread 'Thread 1' value = <Client for Thread 1>
...

```

In the case of **Singleton instance** implementation, the instance variable `@client` is modified concurrently by 2 threads during the threads' executions, so the value in thread `Thread 0` of `@client` is wrong.

In parallel programming, when you have more than one threads potentially modify the same instance variable of an object during the execution, you must use `mutex lock` to lock the instance variable when a thread modifies it, and release the lock only when the execution is done. Then you have to [handle the **dead lock** conditions](https://www.tutorialspoint.com/ruby/ruby_multithreading.htm) correctly. Otherwise you will have instance variables at unexpected values and states. It will make your singleton instance overly complicated with all the `mutex lock` and **dead lock** handling. If you are not an expert in parallel programming, Operating System architecture and Compiler/Interpreter implementation, chances are you will create bugs that infinitely hard to debug when you implement `mutex lock` and **dead lock** handling yourself.

The easiest way to avoid writing `mutex lock` and **dead lock** handling is `share no mutable state`, i.e. `use no mutable instance variable`. And when you use no instance variable, you don't have to create an instance of a class. In this case, use [a service class with class methods](https://github.com/linhchauatl/ruby_threads/blob/master/class_methods.rb) is a much better implementation.

So when should we use [Singleton design pattern](https://en.wikipedia.org/wiki/Singleton_pattern)?

Here are a few rules of thumb

- Use Singleton design pattern:
Read-only or initialized-once config objects: Use in cases of ApplicationConfig, DatabaseConfig ...etc..., when we initialize them only once at the beginning of program execution, then never modify them during runtime.
<br/>

- Avoid Singleton design pattern:
Services that have things that will be changed at runtime. For example a Service that has a `remote_client` that has URL and parameters set from outside by the other executions/objects that call the Service. In this case, if you using Singleton pattern and set the `remote_client` as an instance variable `@remote_client`, you must use `mutex lock` to prevent it to be concurrently modified by more than one threads/processes. It is better to avoid the whole situation altogether by using **No instance with class methods**.
<br/>

Of course, those are just rules of thumb, and please feel free to trouble yourself with writing `mutex lock` and **dead lock** handling yourself if you have compelling reasons to do so.


> Although this article is illustrated using Ruby, it is true for all other Object Oriented Programming languages. Whenever you have objects with instance variables, if you want to use them in places where multiple threads can access your objects at the same time, you must make sure that they are thread-safe, i.e. only one thread can modify those instance variables at any point of time.
> 
> It is especially true with Singleton design pattern, because in this case, you will have only one instance of a class that can be used/invoked/modified by multiple threads in a system. If your Singleton instance has mutable instance variables, carefully use "mutext lock". Otherwise, change your design not to use Singleton design pattern.


**Food for thought:**

1) Reimplement the [Singleton design pattern in singleton_instance.rb](https://github.com/linhchauatl/ruby_threads/blob/master/singleton_instance.rb) using `mutex lock`, to make the output for `***** @client in thread 'Thread x' value = ` in the test display correct values for both `Thread 0` and `Thread 1`. See the `Duration` in this case to see how long it takes to run.

2) Identify all the dead lock conditions in the use of `mutex lock` above, and how to handle them correctly.
