require_relative './class_methods'
require_relative './singleton_instance'

puts "1) Test Class methods ..."
start_time = Time.now
puts "Started at #{start_time}"

threads = []
2.times do |i|
  threads << Thread.new do
    RunProcess.execute("Thread #{i}")
  end
end

threads.each { |thread| thread.join }

end_time = Time.now
puts "End at #{end_time}"
puts "Duration: #{end_time.to_i - start_time.to_i}\n\n"


puts "2) Test Singleton instance ..."
start_time = Time.now
puts "Started at #{start_time}"

threads = []
2.times do |i|
  threads << Thread.new do
    instance = RunProcessSingleton.instance
    instance.execute("Thread #{i}")
  end
end

threads.each { |thread| thread.join }

end_time = Time.now
puts "End at #{end_time}"
puts "Duration: #{end_time.to_i - start_time.to_i}\n\n"
